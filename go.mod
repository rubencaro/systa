module systa

go 1.18

require fyne.io/systray v1.9.1-0.20220508132247-214b548ccb52

require (
	github.com/godbus/dbus/v5 v5.0.4 // indirect
	golang.org/x/sys v0.0.0-20200515095857-1151b9dac4a9 // indirect
)
