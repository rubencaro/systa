package main

import (
	"fmt"
	"io/ioutil"
	"time"

	"fyne.io/systray"
)

func main() {
	systray.Run(onReady, onExit)
}

func onExit() {
	now := time.Now()
	fmt.Println("Exit at", now.String())
}

func readFile(s string) []byte {
	b, err := ioutil.ReadFile(s)
	if err != nil {
		fmt.Print(err)
	}
	return b
}

func onReady() {
	systray.SetIcon(readFile("systa.png"))
	systray.SetTitle("Awesome App")
	mQuit := systray.AddMenuItem("Quit", "Quit the whole app")

	go func() {
		<-mQuit.ClickedCh
		fmt.Println("Requesting quit")
		systray.Quit()
		fmt.Println("Finished quitting")
	}()
}
